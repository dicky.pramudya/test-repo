<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Ensurance</title>
<meta name="description" content="">
<meta name="author" content="">

<!-- Favicons
    ================================================== -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
<link rel="apple-touch-icon" href="img/apple-touch-icon.png">
<link rel="apple-touch-icon" sizes="72x72" href="img/apple-touch-icon-72x72.png">
<link rel="apple-touch-icon" sizes="114x114" href="img/apple-touch-icon-114x114.png">

<!-- Bootstrap -->
<link rel="stylesheet" type="text/css"  href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="fonts/font-awesome/css/font-awesome.css">

<!-- Slider
    ================================================== -->
<link href="css/owl.carousel.css" rel="stylesheet" media="screen">
<link href="css/owl.theme.css" rel="stylesheet" media="screen">

<!-- Stylesheet
    ================================================== -->
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/nivo-lightbox/nivo-lightbox.css">
<link rel="stylesheet" type="text/css" href="css/nivo-lightbox/default.css">
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
<!-- Navigation
    ==========================================-->
<nav id="menu" class="navbar navbar-default navbar-fixed-top">
  <div class="container"> 
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      <a class="navbar-brand page-scroll" href="#page-top">Ensurance</a> </div>
    
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about" class="page-scroll">About</a></li>
        <li><a href="#services" class="page-scroll">Services</a></li>
        <li><a href="#portfolio" class="page-scroll">Gallery</a></li>
        <li><a href="#testimonials" class="page-scroll">Testimonials</a></li>
        <li><a href="#contact" class="page-scroll">Contact</a></li>
      </ul>
    </div>
    <!-- /.navbar-collapse --> 
  </div>
  <!-- /.container-fluid --> 
</nav>
<!-- Header -->
<header id="header">
  <div class="intro">
    <div class="overlay">
      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2 intro-text">
            <h1>Insurance Services</h1>
            <p>Menyelesaikan segala masalah kehidupan dengan asuransi.</p>
            <a href="#about" class="btn btn-custom btn-lg page-scroll">More Info</a> </div>
        </div>
      </div>
    </div>
  </div>
</header>
<!-- About Section -->
<div id="about">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-6">
        <div class="about-text">
          <h2>Selamat Datang di <span>Ensurance</span></h2>
          <hr>
          <p>Kebutuhan akan asuransi setiap harinya akan meningkat mengingat semakin banyaknya ancaman kejahatan maupun bencana alam yang memberikan rasa tidak aman dan tidak nyaman kepada seluruh khalayak manusia.</p>
          <p>Ensurance merupakan sebuah perusahaan asuransi yang melayani segala macam asuransi yang berkaitan dengan anggota tubuh, kepemilikan, aset, maupun barang - barang yang berupa beban.</p>
          <a href="#services" class="btn btn-custom btn-lg page-scroll">View All Services</a> </div>
      </div>
      <div class="col-xs-12 col-md-3">
        <div class="about-media"> <img src="img/about-1.jpg" alt=" "> </div>
        <div class="about-desc">
          <h3>Mental Health Insurance</h3>
          <p>Sayangi kesehatan mental Anda dengan mengasuransikannya dan memberikan diri Anda sedikit terapi.</p>
        </div>
      </div>
      <div class="col-xs-12 col-md-3">
        <div class="about-media"> <img src="img/about-2.jpg" alt=" "> </div>
        <div class="about-desc">
          <h3>Pregnancy Insurance</h3>
          <p>Berikan keamanan dan kenyamanan pada diri Anda yang sedang hamil dengan cara mengasuransikannya.</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Services Section -->
<div id="services">
  <div class="container">
    <div class="col-md-10 col-md-offset-1 section-title text-center">
      <h2>Our Services</h2>
      <hr>
      <p>Berikut merupakan beberapa layanan asuransi kami.</p>
    </div>
    <div class="row">
      <div class="col-md-3 text-center">
        <div class="service-media"> <img src="img/services/service-1.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>Asuransi Kebun</h3>
          <p></p>
        </div>
      </div>
      <div class="col-md-3 text-center">
        <div class="service-media"> <img src="img/services/service-2.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>Asuransi Bunga</h3>
          <p></p>
        </div>
      </div>
      <div class="col-md-3 text-center">
        <div class="service-media"> <img src="img/services/service-3.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>Asuransi Pohon</h3>
          <p></p>
        </div>
      </div>
      <div class="col-md-3 text-center">
        <div class="service-media"> <img src="img/services/service-4.jpg" alt=" "> </div>
        <div class="service-desc">
          <h3>Asuransi Jalan</h3>
          <p></p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Gallery Section -->
<div id="portfolio">
  <div class="container">
    <div class="section-title text-center center">
      <h2>Project Gallery</h2>
      <hr>
      <p>Berikut merupakan beberapa produk yang diasuransikan oleh klien</p>
    </div>
    <div class="categories">
      <ul class="cat">
        <li>
          <ol class="type">
            <li><a href="#" data-filter="*" class="active">All</a></li>
            <li><a href="#" data-filter=".lawn">Asuransi Lahan</a></li>
            <li><a href="#" data-filter=".garden">Asuransi Kebun</a></li>
            <li><a href="#" data-filter=".planting">Asuransi Tanaman</a></li>
          </ol>
        </li>
      </ul>
      <div class="clearfix"></div>
    </div>
    <div class="row">
      <div class="portfolio-items">
        <div class="col-sm-6 col-md-4 lawn">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/01-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/01-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 planting">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/02-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/02-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 lawn">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/03-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/03-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 lawn">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/04-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/04-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 planting">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/05-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/05-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 garden">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/06-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/06-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 garden">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/07-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/07-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 lawn">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/08-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/08-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
        <div class="col-sm-6 col-md-4 planting">
          <div class="portfolio-item">
            <div class="hover-bg"> <a href="img/portfolio/09-large.jpg" title="Project Title" data-lightbox-gallery="gallery1">
              <div class="hover-text">
                <h4>Foto</h4>
              </div>
              <img src="img/portfolio/09-small.jpg" class="img-responsive" alt="Project Title"> </a> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Testimonials Section -->
<div id="testimonials" class="text-center">
  <div class="overlay">
    <div class="container">
      <div class="section-title">
        <h2>Testimoni</h2>
        <hr>
      </div>
      <div class="row">
        <div class="col-md-10 col-md-offset-1">
          <div id="testimonial" class="owl-carousel owl-theme">
            <div class="item">
              <p>"Dengan menggunakan layanan asuransi ini hidup saya lebih aman dan tentram."</p>
              <p>- Sudibyo, Malang, Jawa Timur</p>
            </div>
            <div class="item">
              <p>"Asuransi ini menyelesaikan segala macam permasalahan saya dalam hidup"</p>
              <p>- Ahmad, Surabaya, Jawa Timur</p>
            </div>
            <div class="item">
              <p>"Biasa saja"</p>
              <p>- Muharto, Bogor, Jawa Barat</p>
            </div>
            <div class="item">
              <p>"Keren"</p>
              <p>- Made, Denpasar, Bali</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Contact Section -->
<div id="contact" class="text-center">
  <div class="container">
    <div class="section-title text-center">
      <h2>Tentang Kami</h2>
      <hr>
      <p></p>
    </div>
    <div class="col-md-10 col-md-offset-1 contact-info">
      <div class="col-md-4">
        <h3>Alamat</h3>
        <hr>
        <div class="contact-item">
          <p>Griya Shanta i-243,</p>
          <p>Malang, Jawa Timur 65142</p>
        </div>
      </div>
      <div class="col-md-4">
        <h3>Jadwal Harian</h3>
        <hr>
        <div class="contact-item">
          <p>Senin - Sabtu: 07:00 - 18:00</p>
          <p>Minggu Tutup</p>
        </div>
      </div>
      <div class="col-md-4">
        <h3>Kontak Perusahaan</h3>
        <hr>
        <div class="contact-item">
          <p>Whatsapp: 081234567890</p>
          <p>Email: Email@gmail.com</p>
        </div>
      </div>
    </div>
    <div class="col-md-12">
       <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th scope="col">Name</th>
              <th scope="col">Address</th>
              <th scope="col">Email</th>
              <th scope="col">Status</th>
             </tr>
          </thead>
          <tbody>
            <?php 
              include "koneksi.php"; //memanggil file connect.php
              $query_mysql = mysqli_query($conn, "select * from tb_cp")or die(mysqli_error($conn)); //mengambil isi database
              $nomor = 1; 
              while($data = mysqli_fetch_array($query_mysql)){ //memecah database menjadi field yang ada di dalamnya
            ?>
            <tr>
               <th scope="row"><?php echo $nomor++ ?></th>
               <td><?php echo $data['Name']?></td> <!-- menampilkan isi field -->
               <td><?php echo $data['Address']?></td> <!-- menampilkan isi field -->
               <td><?php echo $data['Email']?></td> <!-- menampilkan isi field -->
               <td><?php echo $data['Status']?></td> <!-- menampilkan isi field -->
            </tr>
          <?php 
          }
          ?>
       </tbody>
    </table>
    <div class="col-md-8 col-md-offset-2">
      <h3>Kritik dan Saran</h3>
      <form name="sentMessage" id="contactForm" novalidate>
        <div class="row">
          <div class="col-md-6">
            <div class="form-group">
              <input type="text" id="name" class="form-control" placeholder="Name" required="required">
              <p class="help-block text-danger"></p>
            </div>
          </div>
          <div class="col-md-6">
            <div class="form-group">
              <input type="email" id="email" class="form-control" placeholder="Email" required="required">
              <p class="help-block text-danger"></p>
            </div>
          </div>
        </div>
        <div class="form-group">
          <textarea name="message" id="message" class="form-control" rows="4" placeholder="Message" required></textarea>
          <p class="help-block text-danger"></p>
        </div>
        <div id="success"></div>
        <button type="submit" class="btn btn-custom btn-lg">Send Message</button>
      </form>
    </div>
  </div>
</div>
<!-- Footer Section -->
<div id="footer">
  <div class="container text-center">
    <div class="col-md-8 col-md-offset-2">
      <div class="social">
        <ul>
          <li><a href="#"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
        </ul>
      </div>
      <?php 

        $url = file_get_contents('https://api.kawalcorona.com/indonesia');
        $data = json_decode($url, true);

      ?>
      <div class="container">
        <h4 class="text-uppercase mb-4">INFO COVID-19 INDONESIA</center></h4>
      
      <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="bg-primary box text-white">
            <div class="row">
              <div class="col-md-3">
                <h5>Positif</center></h5>
                <h2><?php  echo $data[0] ['positif'] ?></h2>
                <h5>Jiwa</h5>
              </div>
              
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="bg-success box text-white">
            <div class="row">
              <div class="col-md-3">
                <h5>Sembuh</h5>
                <h2><?php  echo $data[0] ['sembuh'] ?></h2>
                <h5>Jiwa</h5>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-3">
          <div class="bg-danger box text-white">
            <div class="row">
              <div class="col-md-3">
                <h5>Meninggal</h5>
                <h2><?php  echo $data[0] ['meninggal'] ?></h2>
                <h5>Jiwa</h5>
              </div>
            </div>
          </div>
        </div>
        </div>
      </div>
      <p>&copy; 2016 Landscaper. Designed by <a href="http://www.templatewire.com" rel="nofollow">TemplateWire</a></p>
    </div>
  </div>
</div>
<script type="text/javascript" src="js/jquery.1.11.1.js"></script> 
<script type="text/javascript" src="js/bootstrap.js"></script> 
<script type="text/javascript" src="js/SmoothScroll.js"></script> 
<script type="text/javascript" src="js/nivo-lightbox.js"></script> 
<script type="text/javascript" src="js/jquery.isotope.js"></script> 
<script type="text/javascript" src="js/owl.carousel.js"></script> 
<script type="text/javascript" src="js/jqBootstrapValidation.js"></script> 
<script type="text/javascript" src="js/contact_me.js"></script> 
<script type="text/javascript" src="js/main.js"></script>
</body>
</html>